package com.paul.hello;

import java.util.ArrayList;
import java.util.List;

/**
 * 备选课程类
 */
public class ListTest {
    // 用于存放备选课程的List
    public List coursesToSelect;

    public ListTest() {
        this.coursesToSelect = new ArrayList();
    }

    /**
     * 用于往coursesToSelect中添加课程
     */
    public void testAdd() {
        // 创建一个课程对象，并听过调用add方法，添加到备选课程List中
        Course cr1 = new Course("1", "数据结构");
        coursesToSelect.add(cr1);

        // 对象存入集合都会变成Object类型，取出的时候需要类型转换
        Course temp = (Course) coursesToSelect.get(0);
        System.out.println("添加的课程为：" + temp.getId() + ":" + temp.getName());
    }

    public static void main(String[] args) {
        ListTest list = new ListTest();
        list.testAdd();
    }
}
