package com.paul.hello;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Demo18 {
    public static void main(String[] args) throws ParseException {
        // 使用默认的构造方法创建Date对象，Date的默认无参构造方法创建的对象是当前时间
        Date today = new Date();

        // 输出：Mon Feb 18 11:28:33 CST 2019
        System.out.println(today);

        /**
         * 转换为类似2019-02-18 11:34:18格式的两种方法：
         *
         *  1. 使用format方法将日期转换为指定格式的文本
         *  2. 使用parse方法将文本转换为日期
         */

        // 创建SimpleDateFormat对象，指定目标格式
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // 调用format方法，格式化时间，转换为指定格式字符串
        System.out.println(sdf1.format(today));
        System.out.println(sdf2.format(today));
        System.out.println(sdf3.format(today));

        String day = "2019年02月18日 12:30:40";
        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

        // 调用parse方法，将字符串转换为日期
        Date date = df.parse(day);

        System.out.println(date);

    }
}
