package com.paul.hello;

public class Demo13 {
    public static void main(String[] args) {
        // 创建StringBuilder对象，存储字符串
        StringBuilder str = new StringBuilder("hello");

        // 在字符串后面追加字符串
        str.append(" hongboli");

        // 在字符串后面添加整数
        str.append(520);

        System.out.println("字符串长度： " + str.length());
        System.out.println("字符串为： " + str);

        // 在指定位置插入内日那个
        str.insert(17, "!");

        // 转换为String对象
        String str2 = str.toString();
        System.out.println("字符串为：" + str2);
    }
}
