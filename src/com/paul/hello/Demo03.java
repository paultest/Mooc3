package com.paul.hello;

public class Demo03 {
    public static void main(String[] args) {
        int result1 = test1(10);
        System.out.println("test1方法执行完毕，结果为：" + result1);

        int result2 = test2(10);
        System.out.println("test2方法执行完毕，结果为：" + result2);

        int result3 = test3(10);
        System.out.println("test3方法执行完毕，结果为：" + result3);
    }

    /**
     * divider 除数
     * result 结果
     * try-catch 捕获while 循环
     * 每次循环，divider减1，result = result + 100 / divider
     * 如果：捕获异常，打印输出“抛出异常了”，返回-1
     * 否则：返回result
     *
     * @return
     */
    public static int test1(int divider) {
        int result = 0;
        try {
            while (divider > -1) {
                result = result + 100 / divider;
                divider--;
            }
            return result;
        } catch (Exception e) {
            // 打印异常信息
            e.printStackTrace();
            System.out.println("test1方法循环抛出异常了");
            return -1;
        }
    }

    /**
     * divider 除数
     * result 结果
     * try-catch 捕获while 循环
     * 每次循环，divider减1，result = result + 100 / divider
     * 如果：捕获异常，打印输出“抛出异常了”，返回result = 999
     * 否则：返回result
     * finally：打印输出，同时打印result的值
     *
     * @return
     */
    public static int test2(int divider) {
        int result = 0;
        try {
            while (divider > -1) {
                result = result + 100 / divider;
                divider--;
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("test2方法循环抛出异常了");
            return result = 999;
        } finally {
            System.out.println("这个是finally");
            System.out.println("result的值是" + result);
        }
    }

    /**
     * divider 除数
     * result 结果
     * try-catch 捕获while 循环
     * 每次循环，divider减1，result = result + 100 / divider
     * 如果：捕获异常，打印输出“抛出异常了”，返回1111
     * 否则：返回result
     * finally：打印输出，同时打印result的值
     *
     * @return
     */
    public static int test3(int divider) {
        int result = 0;
        try {
            while (divider > -1) {
                result = result + 100 / divider;
                divider--;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("test3方法循环抛出异常了");
        } finally {
            System.out.println("这个是finally");
            System.out.println("result的值是" + result);
        }
        System.out.println("test方法运行完成");
        return 1111;
    }
}
