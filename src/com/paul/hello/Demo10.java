package com.paul.hello;

public class Demo10 {
    public static void main(String[] args) {
        String str = "学习 JAVA 编程";

        System.out.println("转换为小写： " + str.toLowerCase());

        System.out.println("获取索引为1位置的字符： " + str.charAt(1));

        // 将字符串转换为数组
        byte[] b = str.getBytes();
        System.out.println("转换为字节数组： ");
        for (int i = 0; i < b.length; i++) {
            System.out.print(b[i] + " ");
        }

        System.out.println();
        String str2 = new String("学习 JAVA 编程");
        System.out.println("str和str2的内存地址是否相同？" + (str == str2));
        System.out.println("str和str2的内容是否相同？" + str.equals(str2));
    }
}
