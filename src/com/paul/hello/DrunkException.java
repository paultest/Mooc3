/**
 * 自定义异常类
 */
package com.paul.hello;

public class DrunkException extends Exception {
    public DrunkException() {

    }

    public DrunkException(String message) {
        super(message);
    }
}

