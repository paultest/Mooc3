package com.paul.hello;

import java.util.Arrays;

public class Demo08 {
    public static void main(String[] args) {
        String str = "I am a chinese";
        System.out.println("字符串长度： " + str.length());
        char a = 'a';
        System.out.println("字符a的位置： " + str.indexOf(a));
        System.out.println("字符chinese的位置： " + str.indexOf("chinese"));
        System.out.println("字符I的位置： " + str.indexOf("I"));

        // 按照空格把字符串拆分为一个数组
        String[] arr = str.split(" ");
        System.out.println("按空格拆分成数组： " + Arrays.toString(arr));
        System.out.println();

        // 获取索引位置[3,7)之间的字符串
        System.out.println("获取位置[3,7)之间的字符串： " + str.substring(3, 7));
    }
}
