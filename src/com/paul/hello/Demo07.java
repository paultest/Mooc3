package com.paul.hello;

public class Demo07 {
    public static void main(String[] args) {
        String s1 = "imooc";
        String s2 = "imooc";
        String s3 = new String("imooc");
        String s4 = new String("imooc");

        // 多次出现的字符常量，Java编译程序只创建一个，所以返回true
        System.out.println(s1 == s2);

        // s1和s3是不同的对象，所以返回false
        System.out.println(s1 == s3);

        // s1和s3是不同的对象，所以返回false
        System.out.println(s3 == s4);

        // 字符串s1被修改，指向新的内存空间
        s1 = "new " + s1;
        System.out.println(s1);
    }
}
