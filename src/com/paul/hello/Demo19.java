package com.paul.hello;

import java.util.Calendar;
import java.util.Date;

public class Demo19 {
    public static void main(String[] args) {
        // 创建Calendar对象
        Calendar obj = Calendar.getInstance();

        int year = obj.get(Calendar.YEAR);

        // 获取月份，月份里面0表示1月份的，所以需要加1
        int month = obj.get(Calendar.MONTH) + 1;

        int day = obj.get(Calendar.DAY_OF_MONTH);
        int hour = obj.get(Calendar.HOUR_OF_DAY);
        int minute = obj.get(Calendar.MINUTE);
        int second = obj.get(Calendar.SECOND);

        System.out.println("当前时间为： " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);

        Date date = obj.getTime();
        System.out.println("当前时间为： " + date);

        Long time = obj.getTimeInMillis();
        System.out.println("当前毫秒数为： " + time);
    }
}
