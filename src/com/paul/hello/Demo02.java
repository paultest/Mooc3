package com.paul.hello;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Demo02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            System.out.print("请输入第一个数：");
            int one = input.nextInt();
            System.out.print("请输入第二个数：");
            int two = input.nextInt();
            System.out.println("两数相除结果为：" + (one / two));
        } catch (InputMismatchException e) {
            System.out.println("你应该输入整数");
        } catch (ArithmeticException e) {
            System.out.println("除数不能为0");
        } catch (Exception e) {
            System.out.println("程序异常了");
        }
        System.out.println("程序结束啦");
    }
}
