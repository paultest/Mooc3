package com.paul.hello;

public class Demo05 {
    public static void main(String[] args) {
        Demo05 test = new Demo05();
        try {
            test.test2();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void test2() {
        try {
            test1();
        } catch (DrunkException e) {
            RuntimeException newExc = new RuntimeException("新的捕获到的异常：" + e.getMessage());
            // newExc.initCause(e);
            throw newExc;
        }
    }

    /**
     * @throws DrunkException
     */
    public void test1() throws DrunkException {
        throw new DrunkException("喝酒别开车异常");
    }

}
