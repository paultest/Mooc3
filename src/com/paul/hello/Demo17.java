package com.paul.hello;

public class Demo17 {
    public static void main(String[] args) {
        /* 基本类型转换为字符串有三种方法：
         *  1. 使用包装类的 toString() 方法
         *  2. 使用String类的 valueOf() 方法
         *  3. 用一个空字符串加上基本类型，得到的就是基本类型数据对应的字符串
         */
        int a = 10;
        String str1 = Integer.toString(a);
        String str2 = String.valueOf(a);
        String str3 = a + "";
        System.out.println("str1为：" + str1);
        System.out.println("str2为：" + str2);
        System.out.println("str3为：" + str3);

        /*
         * 字符串转换成基本类型有两种方法：
         *  1. 调用包装类的 parseXxx 静态方法
         *  2. 调用包装类的 valueOf() 方法转换为基本类型的包装类，会自动拆箱
         */
        String str = "8";
        int b = Integer.parseInt(str);
        int c = Integer.valueOf(str);
        System.out.println("b为：" + b);
        System.out.println("c为：" + c);
    }
}
