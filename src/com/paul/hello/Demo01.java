/**
 * try：负责捕获异常，一旦try中发现异常，程序的控制权将被移交给catch块中的异常处理程序
 * <p>
 * catch：如何处理异常？比如发出警告：提示、检查配置、网络连接，记录错误等。执行完catch块之后程序跳出catch块，继续执行后面的代码
 * 多重异常处理代码块顺序问题：先子类再父类（顺序不对也会提醒错误），finally语句块处理最终将要执行的代码
 * 注意：可以多个catch块处理，要按照西安catch子类后再catch父类的处理方法，也就是说先小后大，这是因为就近处理异常（由上而下）
 * <p>
 * finally：最终执行的代码，用于关闭和释放资源等
 */
package com.paul.hello;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Demo01 {
    public static void main(String[] args) {
        try {
            System.out.print("请输入你的年龄：");
            Scanner input = new Scanner(System.in);
            int age = input.nextInt();
            System.out.println("十年后你的年龄为：" + (age + 10));
        } catch (InputMismatchException e) {
            System.out.println("你应该输入整数！！！");
        } catch (Exception e) {
            System.out.println("程序出错啦");
        }
        System.out.println("程序结束啦");
    }
}
