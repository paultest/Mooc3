package com.paul.hello;

public class Demo04 {
    public static void main(String[] args) {
        try {
            int result = divideOuter(5, 0);
            System.out.println("两数相除，结果为：" + result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 两数相除(外部)
     * 注意：该方法不处理异常，继续往上层抛出异常
     *
     * @param one
     * @param two
     * @return
     * @throws Exception
     */
    public static int divideOuter(int one, int two) throws Exception {
        return divideInner(one, two);
    }

    /**
     * 两数相除(内部)
     *
     * @param one
     * @param two
     * @return int
     */
    public static int divideInner(int one, int two) throws Exception {
        if (two == 0) {
            throw new Exception("两数相除，除数不能为0");
        } else {
            return one / two;
        }
    }
}
