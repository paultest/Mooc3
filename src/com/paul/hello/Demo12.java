package com.paul.hello;

public class Demo12 {
    public static void main(String[] args) {

        // 创建一个空的StringBuilder对象
        StringBuilder str1 = new StringBuilder();

        // 创建一个StringBuilder对象，用来存储字符串
        StringBuilder str2 = new StringBuilder("imooc");

        System.out.println(str2);
    }
}