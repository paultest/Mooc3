package com.paul.hello;

public class Demo20 {
    public static void main(String[] args) {
        double a = 12.81;

        int b = (int) a;
        System.out.println("强制类型转换： " + b);

        // 调用round方法，进行四舍五入
        long c = Math.round(a);
        System.out.println("四舍五入： " + c);

        // 调用floor方法，返回小于参数的最大整数
        double d = Math.floor(a);
        System.out.println("floor： " + d);

        // 调用ceil方法，返回大于参数的最小整数
        double e = Math.ceil(a);
        System.out.println("ceil： " + e);

        // 调用random方法，产生[0,1)之间的随机数浮点数
        double x = Math.random();
        System.out.println("随机数： " + x);

        // 产生[0,99)之间的随机整数
        int y = (int) (Math.random() * 99);
        System.out.println("产生[0,99)之间的随机整数： " + y);

        // 定义一个包含10 个元素的整型数组，通过随机产生 10 以内的随机数，给数组中的每个元素赋值，并输出结果。
        // 定义一个整型数组，长度为10
        int[] nums = new int[10];

        //通过循环给数组赋值
        for (int i = 0; i < nums.length; i++) {
            // 产生10以内的随机数
            nums[i] = (int) (Math.random() * 10);// 为元素赋值
        }

        // 使用foreach循环输出数组中的元素
        for (int num : nums) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
